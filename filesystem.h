#include <iostream>
#include <fstream>
#include <string>

std::string writeFileInput();
void readFile(std::string);
void writeFile(std::string);

//Dosyayi Ac
void openFile(std::string filename, int readOrWrite)
{
    switch (readOrWrite)
    {
    case 0:
        readFile(filename);
        break;
    case 1:
        writeFile(filename);
        break;
    default:
        std::cout << "\nError: openFile(\"filename\", " << readOrWrite << ")" << std::endl;
        std::cout << "----------------------------^, Sadece 0 ve 1 degerleri verilebilir.\n\n";
        break;
    }
    
}

//Dosyayi Oku
void readFile(std::string filename)
{
    std::ifstream file;
    std::string output;

    file.open(filename);
        while (!file.eof())
        {
            std::getline(file, output);
            std::cout << output;
        }
    file.close();
}

//Dosyaya Yaz
void writeFile(std::string filename)
{
    std::ofstream file;
    file.open(filename);
        if (file.is_open())
        {
            file << writeFileInput();   
        }
        else
            std::cout << "Dosya Acilamadi."; 

    file.close();
}

//Kullanicidan giris al writeFile ile dosyaya yaz.
std::string writeFileInput()
{
    std::string input;
    char choice;
    
    std::cout << "\nDosyaya ne yazilsin ?: "; std::getline(std::cin, input);
    std::cout << "\nDosyanin eski hali degistirilecek ve sifirlanacak devam edilsin mi ?(e/h): "; std::cin >> choice;
        if (choice == 'e')
        {
            std::cout << "Metin dosyaya basariyla islendi.\n";
            return input;
        }
        else
        {
            exit(0);
        }
        
}